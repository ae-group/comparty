{
#==================================
#---- OVERWRITING ae.gui_help -----
"help_app_state#sound_volume": """\
lautstärke ändern.

tippe ganz links um
alle akustischen
signale zu unterdrücken.

aktuelle lautstärke: {round(self.value * 100)}%

In dieser demo app beeinflusst
dieser Wert zugleich die
Transparenz der Animation.""",
#----------------------------------
#---- OVERWRITING ae.gui_help -----
"help_app_state#vibration_volume": """\
vibrations-intensität

tippe ganz links um
alle vibrationen zu
unterdrücken.

aktuelle vibration: {round(self.value * 100)}%

In dieser demo app beeinflusst
dieser Wert zugleich den
Farbkontrast der Animation.""",
#==================================
"help_flow#change_current:current_object": """\
Tippe um eine datei/nachricht für den
nächsten transfer auszuwählen.""",
"help_flow#change_current:current_remote": """\
Tippe um eine IP adresse auszuwählen
an welche die aktuelle datei/nachricht
gesendet werden soll.""",
"help_flow#confirm_request_tasks_clean": """\
Tippe für optionen zum löschen
von abgeschlossenen transfers.""",
"help_flow#edit_current_object": """\
Eingabefeld um den datei-pfad oder
die nachricht zu korrigieren.""",
"help_flow#edit_current_remote": """\
Eingabefeld um die aktuelle
IP adresse zu korrigieren.""",
"help_flow#open_iterable_displayer": """\
Log- oder sideloading-eintrag.

Tippe um weitere details
anzuzeigen.""",
"help_flow#open_request_task_menu:recv_file": """\
Empfangene datei.

Tippe um weitere
optionen anzuzeigen.""",
"help_flow#open_request_task_menu:recv_message": """\
Empfangene nachricht.

Tippe um weitere
optionen anzuzeigen.""",
"help_flow#open_request_task_menu:send_file": """\
Gesendete datei.

Tippe um weitere
optionen anzuzeigen.""",
"help_flow#open_request_task_menu:send_message": """\
Gesendete nachricht.

Tippe um weitere
optionen anzuzeigen.""",
"help_flow#select_current_object": """\
Aktuelle datei oder nachricht.

Tippe um eine zuvor gesendete
datei/nachricht auszuwählen.""",
"help_flow#select_current_remote": """\
Aktuelle IP adresse.

Tippe um eine zuvor verwendete
IP adresse auszuwählen.""",
"help_flow#send_object": """\
Tippe um die aktuelle datei
oder nachricht zu senden.""",
"help_flow#toggle_close_on_send": """\
Tippe um das eingabefenster nach
dem Senden {'geöffnet zu lassen' if self.state == 'down' else 'automatisch zu schließen'}.""",
"help_flow#toggle_tool_box": """Tippe um das eingabefenster zu {'öffnen' if self.state == 'down' else 'schließen'}.""",
#==================================
#---- OVERWRITING ae.gui_help -----
"flow_id_ink": "{'farbe der ' if app.landscape else ''}transfer service hintergrund-animation",
"flow_path_ink": "{'farbe der ' if app.landscape else ''}sideloading hintergrund-animation",
"selected_item_ink": "{'farbe der ' if app.landscape else ''} transfer service fortschrittsanzeige",
"unselected_item_ink": "{'farbe der ' if app.landscape else ''} sideloading fortschrittsanzeige",
#----------------------------------
"all": "Alles",
"cancel transfer request": "Dateiübertragung abbrechen",
"ComPartY Lite has no QR Scanner": "ComPartY Lite hat keinen QR Scanner",
"confirm request task type to clear": "Wähle Typ der zu löschenden Log-Einträge",
"file": "Dateiübertragung",
"folders can't be transferred": "Verzeichnisse können nicht übertragen werden",
"local ip": "Lokale ip",
"log": "Log",
"message": "Nachricht",
"prepare resend": "Zur Weiterleitung vorbereiten",
"recover send request": "Dateiübertragung fortsetzen",
"Refresh": "Aktualisiere",
"request task data": "Log-Eintrags-Daten",
}
