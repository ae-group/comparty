""" NOT WORKING android transfer service starter module. """
from ae.inspector import module_file_path
from ae.transfer_service import service_factory

# app start
print(f"exec_of_service.py with __name__='{__name__}'  module_path={module_file_path(local_object=lambda: 0)}")
if __name__ == '__main__':
    server_app = service_factory()
    server_app.run_app()
